﻿using Calculateurs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Expression = Calculateurs.Expression;

namespace Calculatrice
{
    public class ViewModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private Dictionary<string, object> _propertyValues = new Dictionary<string, object>();

        public T GetValue<T>([CallerMemberName] string propertyName = null)
        {
            if (_propertyValues.ContainsKey(propertyName))
                return (T)_propertyValues[propertyName];
            return default(T);
        }
        public bool SetValue<T>(T newValue, [CallerMemberName] string propertyName = null)
        {
            var currentValue = GetValue<T>(propertyName);
            if (currentValue == null && newValue != null
             || currentValue != null && !currentValue.Equals(newValue))
            {
                _propertyValues[propertyName] = newValue;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
                return true;
            }
            return false;
        }

        #endregion

        #region Bindings sortie
        public string Saisie
        {
            get { return GetValue<string>(); }
            set { SetValue(value); }
        }
        public string Resultat
        {
            get { return GetValue<string>(); }
            set { SetValue(value); }
        }

        private ObservableCollection<Historique> _historique;
        public ObservableCollection<Historique> MonHistorique
        {
            get
            {
                if (_historique == null) _historique = new ObservableCollection<Historique>();
                return _historique;
            }

            set
            {
                SetValue(value);
            }
        }
        #endregion

        #region Bindings entrée

        private ICommand numeriqueInput;
        public ICommand NumeriqueInput
        {
            get
            {
                if (numeriqueInput == null)
                {
                    numeriqueInput = new RelayCommand(Button_Click_Num);
                }
                return numeriqueInput;
            }
        }

        private ICommand operateurInput;
        public ICommand OperateurInput
        {
            get
            {
                if (operateurInput == null)
                {
                    operateurInput = new RelayCommand(Button_Click_Operation);
                }
                return operateurInput;
            }
        }

        private ICommand functionInput;
        public ICommand FunctionInput
        {
            get
            {
                if (functionInput == null)
                {
                    functionInput = new RelayCommand(Button_Click_Function);
                }
                return functionInput;
            }
        }

        private ICommand parentheseInput;
        public ICommand ParentheseInput
        {
            get
            {
                if (parentheseInput == null)
                {
                    parentheseInput = new RelayCommand(Button_Click_Parenthese);
                }
                return parentheseInput;
            }
        }

        private ICommand egalInput;
        public ICommand EgalInput
        {
            get
            {
                if (egalInput == null)
                {
                    egalInput =  new RelayCommand(Button_Click_Egal);
                }
                return egalInput;
            }
        }

        private ICommand specialInput;
        public ICommand SpecialInput
        {
            get
            {
                if (specialInput == null)
                {
                    specialInput = new RelayCommand(Button_Click_Special);
                }
                return specialInput;
            }
        }

        private ICommand reinitialiserHistoriqueInput;
        public ICommand ReinitialiserHistoriqueInput
        {
            get
            {
                if (reinitialiserHistoriqueInput == null)
                {
                    reinitialiserHistoriqueInput = new RelayCommand(ReinitialiserHistorique);
                }
                return reinitialiserHistoriqueInput;
            }
        }

        private ICommand rechargerHistoriqueInput;
        public ICommand RechargerHistoriqueInput
        {
            get
            {
                if (rechargerHistoriqueInput == null)
                {
                    rechargerHistoriqueInput = new RelayCommand(RechargerDepuisHistorique);
                }
                return rechargerHistoriqueInput;
            }
        }

        #endregion

        #region Attributs

        private Expression expressionPrincipale;

        #endregion

        public ViewModel()
        {
            expressionPrincipale = new Expression();
            Saisie = "0";
        }

        #region Méthodes

        /// <summary>
        /// Met à jour la TextBox de saisie.
        /// Vide la TextBox de résultat si on passe false en paramètre
        /// </summary>
        /// <param name="viderResultat">Permet d'indiquer si on souhaite remettre à zéro l'affichage du résultat</param>
        private void MajAffichage(bool viderResultat)
        {
            if (viderResultat) Resultat = "";
            Saisie = expressionPrincipale.GetExpression();
        }

        private void Button_Click_Special(object contenu)
        {
            switch (contenu.ToString())
            {
                case "⌫":
                    expressionPrincipale.SupprimerDernierCaractere();
                    break;

                case "C":
                    expressionPrincipale = new Expression();
                    break;

                    /*case "+-":
                        Saisie = Convert.ToString(Convert.ToInt32(Saisie) * -1);
                        break;*/
            }
            MajAffichage(true);
        }

        private void Button_Click_Num(object valeur)
        {
            expressionPrincipale.AjouterChiffre(int.Parse(valeur.ToString()));
            MajAffichage(true);
        }
        private void Button_Click_Operation(object operateur)
        {
            expressionPrincipale.AjouterOperateur(operateur.ToString(), false);
            MajAffichage(true);
        }

        private void Button_Click_Function(object fonction)
        {
            expressionPrincipale.AjouterOperateur(fonction.ToString(), true);
            expressionPrincipale.AjouterParenthese(true);
            MajAffichage(true);
        }

        private void Button_Click_Parenthese(object type)
        {
            bool ouvrante = false;
            if (type.ToString() == "(") ouvrante = true;
            expressionPrincipale.AjouterParenthese(ouvrante);
            MajAffichage(true);
        }

        private void Button_Click_Egal()
        {
            string resultat;
            try
            {
                resultat = expressionPrincipale.Calculer().ToString();
            }
            catch (Exception ex)
            {
                if (ex.Message == "Pile vide.") resultat = "Il n'y a rien à calculer";
                else resultat = ex.Message;
            }
            Resultat = resultat;
            Historique newHisto = new Historique() { ResultatHisto = resultat, SaisieHisto = expressionPrincipale.GetExpression(), ExpressionHisto = expressionPrincipale.Clone() };
            MonHistorique.Add(newHisto);
        }

        private void ReinitialiserHistorique()
        {
            MonHistorique.Clear();
        }

        private void RechargerDepuisHistorique(object sender)
        {
            ListBox boxItem = (ListBox)sender;
            int indexItem = boxItem.SelectedIndex;
            Expression expressionHistorique = MonHistorique[indexItem].ExpressionHisto;
            expressionPrincipale = expressionHistorique;
            MajAffichage(true);
        }

        #endregion
    }
}
