﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using Calculateurs;
using Expression = Calculateurs.Expression;

namespace Calculatrice
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ViewModel modele;
        public MainWindow()
        {
            DataContext = modele = new ViewModel();
            InitializeComponent();
        }

        /// <summary>
        /// Met à jour la TextBox de saisie.
        /// Vide la TextBox de résultat si on passe false en paramètre
        /// </summary>
        /// <param name="viderResultat">Permet d'indiquer si on souhaite remettre à zéro l'affichage du résultat</param>
        private void MajAffichage(bool viderResultat)
        {
            /*if(viderResultat)model.Resultat = "";
            model.Saisie = expressionPrincipale.GetExpression();*/
        }
        
        private void Button_Click_Special(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            modele.SpecialInput.Execute(b.Content);
        }

        private void Button_Click_Num(object sender, RoutedEventArgs e)
        {
            Button b = (Button) sender;
            modele.NumeriqueInput.Execute(b.Content);
        }
        private void Button_Click_Operation(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            modele.OperateurInput.Execute(b.Content);
        }

        private void Button_Click_Function(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            modele.FunctionInput.Execute(b.Content);
        }

        private void Button_Click_Parenthese(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;
            modele.ParentheseInput.Execute(b.Content);
        }

        private void Button_Click_Egal(object sender, RoutedEventArgs e)
        {
            modele.EgalInput.Execute(null);
        }

        private void ReinitialiserHistorique(object sender, RoutedEventArgs e)
        {
            modele.ReinitialiserHistoriqueInput.Execute(null);
        }

        private void RechargerDepuisHistorique(object sender, RoutedEventArgs e)
        {
            modele.RechargerHistoriqueInput.Execute(sender);
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            Console.WriteLine(e.Key);
        }
    }
}
