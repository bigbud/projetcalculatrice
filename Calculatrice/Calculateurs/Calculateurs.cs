﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculateurs
{
    public class Expression
    {
        #region Attributs
        private List<IOperande> lesOperandes;
        public List<IOperande> LesOperandes { get => lesOperandes; set => lesOperandes = value; }
        #endregion

        #region Constructeurs
        public Expression()
        {
            LesOperandes = new List<IOperande>();
        }
        public Expression(List<IOperande> desOperandes)
        {
            LesOperandes = desOperandes;
        }

        /// <summary>
        /// Clone l'expression, en clonant aussi tous les opérandes de la liste d'opérandes
        /// </summary>
        /// <returns>Clone de l'objet courant</returns>
        public Expression Clone()
        {
            List<IOperande> listeClonee = new List<IOperande>();
            foreach(IOperande unOperande in LesOperandes)
            {
                if (unOperande is Nombre) listeClonee.Add((Nombre) unOperande.Clone());
                if (unOperande is Operateur) listeClonee.Add((Operateur)unOperande.Clone());
                if (unOperande is Parenthese) listeClonee.Add((Parenthese)unOperande.Clone());
            }
            Expression leClone = new Expression(listeClonee);
            return leClone;
        }
        #endregion

        #region Méthodes
        /// <summary>
        /// Réalise tous les calculs de l'expression correspondante
        /// </summary>
        /// <returns>Résultat du calcul de l'expression</returns>
        public decimal Calculer()
        {
            List<IOperande> expressionRPN = ShuntingYard.ToRPN(lesOperandes);
            decimal resultat = ShuntingYard.CalculerRPN(expressionRPN);
            return resultat;
        }

        public void SupprimerDernierCaractere()
        {
            if (LesOperandes.Count > 0 && !LesOperandes.Last().SupprimerCaractere())
            {
                LesOperandes.RemoveAt(LesOperandes.Count - 1);
                // Si l'opérande précédente est un opérateur, et que cet opérateur est une fonction, alors c'est qu'on
                // vient de supprimer sa parenthèse ouvrante. Il faut donc supprimer la fonction aussi
                if (LesOperandes.Count > 0 && LesOperandes.Last() is Operateur)
                {
                    Operateur dernierOperateur = (Operateur)LesOperandes.Last();
                    if (dernierOperateur.IsFunction)
                        this.SupprimerDernierCaractere();
                }

            }

        }

        /// <summary>
        /// Ajoute le chiffre correspoondant à la liste des opérandes.
        /// Si le dernier opérande est un chiffre, on concatene l'ancien avec le nouveau.
        /// </summary>
        /// <param name="chiffre"></param>
        public void AjouterChiffre(int chiffre)
        {
            if(LesOperandes.LastOrDefault() is Nombre)
            {
                LesOperandes.Last().AjouterChiffre(chiffre);
            }
            if(LesOperandes.LastOrDefault() is Operateur || LesOperandes.LastOrDefault() is Parenthese || LesOperandes.Count == 0)
            {
                LesOperandes.Add(new Nombre(chiffre));
            }
        }

        /// <summary>
        /// Ajoute l'opérateur correspondant à la liste des opérandes
        /// Si l'opérande précédente est déjà un opérateur, l'ajout n'a pas lieu, à moins que l'opérateur ajouté soit une fonction
        /// </summary>
        /// <param name="operateur">L'opérateur à ajouter</param>
        /// <returns>1 si l'opérateur a été ajouté, sinon 0</returns>
        public bool AjouterOperateur(string operateur, bool isFunction)
        {
            if (LesOperandes.LastOrDefault() is Operateur && !isFunction)
            {
                return false;
            }
            else
            {
                LesOperandes.Add(new Operateur(operateur, isFunction));
                return true;
            }
        }

        public void AjouterParenthese(bool ouvrante)
        {
            LesOperandes.Add(new Parenthese(ouvrante));
        }

        /// <summary>
        /// Permet de récupérer l'expression entière sous forme de chaine de caractère
        /// </summary>
        /// <returns>(String) La liste des opérandes</returns>
        public string GetExpression()
        {
            string lExpression = "";
            foreach(IOperande unOperande in LesOperandes)
            {
                lExpression += unOperande.ToString();
            }
            return lExpression;
        }

        #endregion
    }

    public interface IOperande
    {
        void AjouterChiffre(int leChiffre);
        /// <summary>
        /// Supprime le dernier caractère de l'opérande
        /// Si la chaîne est maintenant vide : retourne false
        /// </summary>
        /// <returns>S'il reste des caractères : true. Si la chaîne est maintenant vide : false</returns>
        bool SupprimerCaractere();
        string ToString();
        object Clone();
    }

    public class Nombre : IOperande
    {
        #region Attributs
        private string valeur;

        public string Valeur { get => valeur; set => valeur = value; }
        #endregion

        public Nombre(string valeur)
        {
            Valeur = valeur;
        }

        public Nombre(int valeur)
        {
            Valeur = valeur.ToString();
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        #region Méthodes
        /// <summary>
        /// Supprime le dernier caractère de la valeur du nombre
        /// Si la chaîne est maintenant vide : retourne false
        /// </summary>
        /// <returns>S'il reste des caractères : true. Si la chaîne est maintenant vide : false </returns>
        public bool SupprimerCaractere()
        {
            Valeur = Valeur.Substring(0, Valeur.Length - 1);
            if (Valeur.Length == 0) return false;
            else return true;
        }

        /// <summary>
        /// Permet de récupérer le nombre en type numérique
        /// </summary>
        /// <returns>La valeur du nombre en type int</returns>
        public int ToInt()
        {
            return int.Parse(Valeur);
        }

        public override string ToString()
        {
            return Valeur;
        }

        public void AjouterChiffre(int leChiffre)
        {
            Valeur += leChiffre;
        }
        #endregion
    }

    public class Operateur : IOperande
    {
        private string type;
        private int priorite;
        private bool rightAssociative;
        private bool isFunction;

        public string Type { get => type; set => type = value; }
        public int Priorite { get => priorite; set => priorite = value; }
        public bool RightAssociative { get => rightAssociative; set => rightAssociative = value; }
        public bool IsFunction { get => isFunction; set => isFunction = value; }

        public Operateur(string type, bool isFunction)
        {
            if (type == "x^") type = "^";
            if (type == "√") type = "Sqrt";

            Type = type;
            RightAssociative = false;
            IsFunction = isFunction;
            switch (Type)
            {
                case "^":
                    Priorite = 4;
                    RightAssociative = true;
                    break;
                case "*":
                case "/":
                    Priorite = 3;
                    break;
                case "+":
                case "-":
                    Priorite = 2;
                    break;
            }
        }
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        /// <summary>
        /// Permet d'indique qu'il faut supprimer cet objet de la liste des Operandes
        /// </summary>
        /// <returns>False dans tous les cas</returns>
        public bool SupprimerCaractere()
        {
            return false;
        }

        public override string ToString()
        {
            return Type;
        }

        public void AjouterChiffre(int leChiffre)
        {
            throw new NotImplementedException();
        }
    }

    public class Parenthese : IOperande
    {
        private bool ouvrante;

        public bool Ouvrante { get => ouvrante; set => ouvrante = value; }

        public Parenthese(bool ouvrante)
        {
            Ouvrante = ouvrante;
        }
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        /// <summary>
        /// Renvoie une parenthèse ouvrante ou fermante
        /// </summary>
        /// <returns>String parenthèse</returns>
        public override string ToString()
        {
            if (Ouvrante) return "(";
            else return ")";
        }

        public void AjouterChiffre(int leChiffre)
        {
            throw new NotImplementedException();
        }

        public bool SupprimerCaractere()
        {
            return false;
        }
    }

    public static class Calcul
    {

        public static decimal Addition(decimal nb1, decimal nb2)
        {
            decimal result = nb1 + nb2;
            return result;
        }
        public static decimal Division(decimal nb1, decimal nb2)
        {
            decimal result;
            if (nb2 != 0) result = nb1 / nb2;
            else throw new Exception("Division par 0 impossible");
            return result;
        }
        public static decimal Multiplication(decimal nb1, decimal nb2)
        {
            decimal result = nb1 * nb2;
            return result;
            
        }
        public static decimal Soustraction(decimal nb1, decimal nb2)
        {
            decimal result = nb1 - nb2;
            return result;
        }
    }

    public static class ShuntingYard
    {
        public static List<IOperande> ToRPN(List<IOperande> expression)
        {
            var pile = new Stack<IOperande>();
            var sortie = new List<IOperande>();
            foreach(IOperande unOperande in expression)
            {
                if (unOperande is Nombre)
                    sortie.Add(unOperande);
                else if (unOperande is Operateur)
                {
                    while(pile.Count > 0)
                    {
                        Operateur operateur1 = (Operateur)unOperande;
                        if (operateur1.IsFunction) break;
                        IOperande operande2 = pile.Peek();
                        if (operande2 is Parenthese) break;
                        Operateur operateur2 = (Operateur)pile.Peek();
                        int comparaison = operateur1.Priorite.CompareTo(operateur2.Priorite);

                        // Si l'opérateur 1 est moins prioritaire que le 2
                        // OU 
                        // Que l'opérateur 1 est leftAssociative ET que sa priorité est égale ou supérieure à celle du 2
                        if (comparaison < 0 || !operateur1.RightAssociative && comparaison <= 0)
                        {
                            // On ajoute d'abord l'opérateur le plus haut à la sortie
                            sortie.Add(pile.Pop());
                        }
                        else
                        {
                            // Sinon on ne fait rien, l'opérateur courant ira au dessus de la pile
                            break;
                        } 
                    }
                    // On ajoute l'opérateur à la pile d'opérateurs
                    pile.Push(unOperande);
                }
                else if (unOperande is Parenthese)
                {
                    Parenthese laParenthese = (Parenthese)unOperande;
                    if(laParenthese.Ouvrante)
                    {
                        pile.Push(unOperande);
                    }
                    else
                    {
                        IOperande top = null;
                        // Tant qu'il y a des éléments dans la pile, et qu'on ne trouve pas une parenthèse ouvrante
                        while(pile.Count > 0 && (top = pile.Pop()).ToString() != "(")
                        {
                            sortie.Add(top);
                        }
                        // Si on ne trouve pas de parenthèse ouvrante dans la pile, il y a une erreur car il manque une parenthèse
                        if (top.ToString() != "(") throw new Exception("Parenthèse ouvrante manquante");
                    }
                }
            }

            while(pile.Count > 0)
            {
                var top = pile.Pop();
                // Si la pile contient une parenthèse, c'est qu'une parenthèse fermante a été oubliée
                // Car elle aurait été envoyée dans la sortie si elle avait trouvé sa paire
                if (top is Parenthese) throw new Exception("Parenthèse fermante manquante");
                sortie.Add(top);
            }

            return sortie;
        }

        public static decimal CalculerRPN(List<IOperande> expression)
        {
            Stack<decimal> pile = new Stack<decimal>();
            decimal nombre = 0;

            foreach(IOperande unOperande in expression)
            {
                if (decimal.TryParse(unOperande.ToString(), out nombre))
                {
                    pile.Push(nombre);
                }
                else
                {
                    decimal nombre1 = 0;
                    decimal nombre2 = 0;
                    // On vérifie que la pile n'est pas vide, sinon on lance une exception
                    if (pile.Count == 0) throw new Exception("Expression non valide");
                    // Si on a un seul nombre dans la pile, on vérifie si l'opérateur est un opérateur prenant un seul nombre.
                    // Si ce n'est pas le cas, on lance une exception
                    else if (pile.Count == 1)
                    {
                        Operateur lOperateur = (Operateur)unOperande;
                        if (lOperateur.IsFunction || unOperande.ToString() == "-") nombre1 = pile.Pop();
                        else throw new Exception("Expression non valide");
                    }
                    else
                    {
                        // On peut avoir deux nombres dans la pile, et pourtant une fonction (s'il y a eu une opération avant)
                        // On prévoit donc ce cas ici, en ne récupérant que le nombre le plus haut sur la pile
                        Operateur lOperateur = (Operateur)unOperande;
                        if (lOperateur.IsFunction)
                            nombre1 = pile.Pop();
                        else
                        {
                            nombre1 = pile.Pop();
                            nombre2 = pile.Pop();
                        }
                    }

                    switch(unOperande.ToString())
                    {
                        case "^":
                            pile.Push((decimal)Math.Pow((double)nombre1, (double)nombre2));
                            break;
                        case "ln":
                            pile.Push((decimal)Math.Log((double)nombre1, Math.E));
                            break;
                        case "Sqrt":
                            pile.Push((decimal)Math.Sqrt((double)nombre1));
                            break;
                        case "Sin":
                            pile.Push((decimal)Math.Sin((double)nombre1));
                            break;
                        case "Cos":
                            pile.Push((decimal)Math.Cos((double)nombre1));
                            break;
                        case "Tan":
                            pile.Push((decimal)Math.Tan((double)nombre1));
                            break;
                        case "*":
                            pile.Push(Calcul.Multiplication(nombre1, nombre2));
                            break;
                        case "/":
                            pile.Push(Calcul.Division(nombre2, nombre1));
                            break;
                        case "+":
                            pile.Push(Calcul.Addition(nombre1, nombre2));
                            break;
                        case "-":
                            pile.Push(Calcul.Soustraction(nombre2, nombre1));
                            break;
                        default:
                            throw new Exception("L'opération '" + unOperande.ToString() + "' n'est pas implémentée");
                    }
                }
            }
            return pile.Pop();
        }
    }
}

