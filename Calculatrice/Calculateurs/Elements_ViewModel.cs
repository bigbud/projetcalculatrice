﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Calculateurs
{
    public class Historique : BaseNotifyPropertyChanged
    {
        public string SaisieHisto
        {
            get { return (string)GetField(); }
            set { SetField(value); }
        }
        public string ResultatHisto
        {
            get { return (string)GetField(); }
            set { SetField(value); }
        }
        public Expression ExpressionHisto
        {
            get { return (Expression)GetField(); }
            set { SetField(value); }
        }
    }

    public class RelayCommand : ICommand
    {
        private readonly Action<object> methodeAExecuter;
        private Func<bool> evaluateurCanExecute;
        private Action procedureAExecuter;

        public RelayCommand(Action<object> methodeAExecuter, Func<bool> evaluateurCanExecute)
        {
            this.methodeAExecuter = methodeAExecuter;
            this.evaluateurCanExecute = evaluateurCanExecute;
        }

        public RelayCommand(Action<object> methodeAExecuter)
        {
            this.methodeAExecuter = methodeAExecuter;
            evaluateurCanExecute = null;
        }

        public RelayCommand(Action procedureAExecuter)
        {
            this.procedureAExecuter = procedureAExecuter;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            if (evaluateurCanExecute == null) return true;
            else
            {
                bool result = evaluateurCanExecute.Invoke();
                return result;
            }
        }

        public void Execute(object parameter)
        {
            if (parameter == null) Execute();
            else this.methodeAExecuter(parameter);
        }

        public void Execute()
        {
            this.procedureAExecuter();
        }
    }
}
